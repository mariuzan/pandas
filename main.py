import numpy as np
import pandas as pd

df = pd.read_csv("https://pycourse.s3.amazonaws.com/bike-sharing.csv")
dataframe = pd.DataFrame(df, columns=df.columns[:-1])
# gam=dataframe.median(['windspeed'][range(0,len(df))])
# print((sum(gam)/2))

# gam2=dataframe['weather_condition'][range(0,len(df))]
#print('vendo so',sum(gam2))

print('Media da velocidade do vento', df['windspeed'].mean())
print('Media da temperatura', df['temp'].mean())
# dataframe['year'].head()
print('Registros apartir de 2011', len(df[df.year == 0]))
print('Registros apartir de 2012', len(df[df.year == 1]))
print('vendo locações 2011', sum(df[df.year == 0].total_count))
print('vendo locações 2012', sum(df[df.year == 1].total_count))
print('vendo a estaão 1', sum(df[df.season == 1].total_count))
print('vendo a estaão 2', sum(df[df.season == 2].total_count))
print('vendo a estaão 3', sum(df[df.season == 3].total_count))
print('vendo a estaão 4', sum(df[df.season == 4].total_count))

print('vendo horario 4', sum(df[df.hour == 4].total_count))
print('vendo horario 17', sum(df[df.hour == 17].total_count))
print('vendo horario 20', sum(df[df.hour == 20].total_count))
print('vendo horario 7', sum(df[df.hour == 7].total_count))

print('vendo dia 4', sum(df[df.weekday == 4].total_count))
print('vendo dia 2', sum(df[df.weekday == 2].total_count))
print('vendo dia 6', sum(df[df.weekday == 6].total_count))
print('vendo dia 0', sum(df[df.weekday == 0].total_count))

print('vendo dia 3 e os horario 21', sum(
    df.query('weekday ==3 & hour==21 ').total_count))
print('vendo dia 3 e os horario 17', sum(
    df.query('weekday ==3 & hour==17 ').total_count))
print('vendo dia 3 e os horario 13', sum(
    df.query('weekday ==3 & hour==13 ').total_count))
print('vendo dia 3 e os horario 7', sum(
    df.query('weekday ==3 & hour==7 ').total_count))

print('vendo dia 6 e os horario 21', sum(
    df.query('weekday ==6 & hour==21 ').total_count))
print('vendo dia 6 e os horario 17', sum(
    df.query('weekday ==6 & hour==17 ').total_count))
print('vendo dia 6 e os horario 13', sum(
    df.query('weekday ==6 & hour==13 ').total_count))
print('vendo dia 6 e os horario 7', sum(
    df.query('weekday ==6 & hour==7 ').total_count))
print('Total de registros', len(df))
# print(df.index)
